<?php

namespace Drupal\concurrent_queue\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drush\Commands\DrushCommands;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use React\Socket\ConnectionInterface;
use React\Socket\Server;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ConcurrentQueueCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueService;

  /**
   * @var LoopInterface
   */
  protected $loop;

  /**
   * @var \React\Socket\ServerInterface
   */
  protected $socket;

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The processed items.
   * @var array
   */
  protected $items = [];

  /**
   * The connected workers.
   * @var ConnectionInterface[]
   */
  protected $workers = [];

  /**
   * The limit of workers.
   * @var int
   */
  protected $maxWorkers = 1;

  /**
   * Current item counter.
   * @var int
   */
  protected $count = 0;
  protected $queueName;


  /**
   * ConcurrentQueueCommands constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueService
   */
  public function __construct(QueueFactory $queueService)
  {
    $this->queueService = $queueService;
  }

  /**
   * Run a specific queue by name.
   *
   * @command concurrent_queue:run
   * @aliases concurrent_queue-run
   * @param string $name The name of the queue to run, as defined in either hook_queue_info or hook_cron_queue_info.
   * @validate-queue name
   * @option workers The maximum number of workers.
   */
  public function run($name, $options = ['workers' => self::OPT])
  {
    if (!empty($options['workers'])) {
      $this->maxWorkers = (int) $options['workers'];
    }
    $this->logger()->info(dt('Loading queue @name', ['@name' => $name]));
    $start = microtime(true);
    $this->queueName = $name;
    $this->queue = $this->queueService->get($name);
    if (!$this->queue->numberOfItems()) {
      $this->logger()->info(dt('Queue @name doesn\'t have any items to process', ['@name' => $name]));
      return;
    }

    $this->logger()->info(dt('Starting queue server...'));
    $this->loop = Factory::create();
    $this->socket = new Server('127.0.0.1:30001', $this->loop);

    $this->bindConnection();
    $this->bindTimers();

    $this->loop->run();
    $elapsed = microtime(true) - $start;
    $this->logger()->success(dt('Processed @count items from the @name queue in @elapsed sec.', ['@count' => $this->count, '@name' => $name, '@elapsed' => round($elapsed, 2)]));
  }

  /**
   *
   */
  public function shutdown() {
    $this->logger()->info(dt('Shutting down @count workers', ['@count' => count($this->workers)]));
    foreach ($this->workers as $index => $worker) {
      if (!$worker instanceof ConnectionInterface) {
        continue;
      }
      if (!$worker->isReadable()) {
        continue;
      }
      $worker->close();
      $this->logger()->info(dt('Worker @id shutdown', ['@id' => $index]));
    }
    $this->loop->stop();
  }

  /**
   *
   */
  protected function bindConnection() {
    $queue = $this->queue;
    $loop = $this->loop;
    $workers = &$this->workers;
    $logger = $this->logger();
    $count = &$this->count;
    $items = &$this->items;
    $queueName = $this->queueName;
    $instance = $this;

    $this->socket->on('connection', function (ConnectionInterface $connection) use ($queue, $loop, $logger, $instance, $queueName, &$workers, &$count, &$items) {
      $logger->info(dt('Worker connecting'));
      $workerId = count($workers);
      $workers[] = $connection;
      $connection->on('data', function ($data) use ($queue, $connection, $loop, $logger, $queueName, $workerId, &$count, &$items) {
        switch ($data) {
          // Worker notify memory limit.
          case 'm':
            $connection->close();
            $this->spawnWorker();
            break;
          // Worker request item.
          case 'r':
            $item = $queue->claimItem();
            if (empty($item)) {
              $logger->info(dt('No items to claim, closing Worker @workerId', [
                '@workerId' => $workerId,
              ]));
              $connection->close();
              break;
            }
            $i = $count++;
            $logger->info(dt('Worker @workerId claimed item @itemId', [
              '@workerId' => $workerId,
              '@itemId' => $i,
            ]));
            $items[$i] = $item;
            $data = [
              'queue' => $queueName,
              'action' => 'queue',
              'i' => $i,
              'item' => $item,
            ];
            $connection->write(serialize($data));
            break;
          default:
            if (is_numeric($data)) {
              $i = (int) $data;
              $logger->info(dt('Worker @workerId finished item @itemId', [
                '@workerId' => $workerId,
                '@itemId' => $i,
              ]));
              $item = $items[$i];
              $queue->deleteItem($item);
            }
            break;
        }
      });
    });
  }

  /**
   *
   */
  public function spawnWorker() {
    $moduleDir = \Drupal::service('file_system')->realpath(
      drupal_get_path('module', 'concurrent_queue')
    );
    $projectDir = dirname(DRUPAL_ROOT);
    $bin = 'php -d memory_limit=2G ';
    $bin .= $projectDir . '/vendor/drush/drush/drush';
    $bin .= ' -r "' . DRUPAL_ROOT . '"';
    $bin .= ' php-script \'queue-worker.php\'';
    $bin .= ' --script-path="' . $moduleDir . '"';
    shell_exec('nohup '.$bin.' > /dev/null 2>&1 & echo $!');
  }

  /**
   * Bind the timers to the loop.
   */
  protected function bindTimers() {
    $queue = $this->queue;
    $loop = $this->loop;
    $workers = &$this->workers;
    $maxWorkers = $this->maxWorkers;
    $items = &$this->items;
    $instance = $this;
    $loop->addPeriodicTimer(.5, function () use ($queue, $loop, $instance, &$workers, &$items) {
      // No workers bound yet, skip
      if (empty($workers)) {
        return;
      }
      $hasActive = FALSE;
      foreach($workers as $worker) {
        if ($worker instanceof ConnectionInterface) {
          $hasActive = $hasActive || $worker->isReadable();
        }
      }
      if (!$hasActive) {
        $instance->shutdown();
      }
    });
    $loop->addTimer(.2, function () use ($maxWorkers, $instance) {
      for ($i = 0; $i < $maxWorkers; $i++) {
        $instance->spawnWorker();
      }
    });
  }
}
