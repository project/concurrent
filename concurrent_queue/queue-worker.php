<?php
$memory_limit = 512 * 1024 * 1024 * 0.95;

$loop = React\EventLoop\Factory::create();
$tcpConnector = new React\Socket\TcpConnector($loop);
$workerManager = \Drupal::service('plugin.manager.queue_worker');
$tcpConnector->connect('127.0.0.1:30001')->then(function (React\Socket\ConnectionInterface $connection) use ($loop, $workerManager, $memory_limit) {
  $connection->write('r');
  $connection->on('data', function ($data) use ($connection, $loop, $memory_limit, $workerManager) {
    $info = unserialize($data);
    switch($info['action']) {
      case 'queue':
        try {
          $worker = $workerManager->createInstance($info['queue']);
          $worker->processItem($info['item']->data);
        }
        catch (\Throwable $exception) {
          // @TODO trigger release.
        }
        // Notify queue processed item.
        $connection->write($info['i']);
        // Verify status on the next tick.
        $loop->addTimer(.1, function () use ($connection, $memory_limit) {
          if (memory_get_usage() > $memory_limit) {
            // Let server know the worker must die...
            $connection->write('m');
          }
          else {
            // Keep going..
            $connection->write('r');
          }
        });
        break;
    }
  });
});

$loop->run();
