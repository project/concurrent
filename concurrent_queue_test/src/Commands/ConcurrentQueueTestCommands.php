<?php

namespace Drupal\concurrent_queue_test\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Queue\QueueFactory;
use Drush\Commands\DrushCommands;
use GuzzleHttp\ClientInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ConcurrentQueueTestCommands extends DrushCommands {


  /**
   * The Queue instance for characters.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $charactersQueue;

  /**
   * The Queue instance for characters.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * ConcurrentQueueTestCommands constructor.
   */
  public function __construct(QueueFactory $queueFactory,  $httpClient) {
    $this->charactersQueue = $queueFactory->get('concurrent_queue_test_swapi_characters', TRUE);
    $this->httpClient = $httpClient;
  }


  /**
   * Fills the queue with items.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.

   * @param $filmId
   *   The film id
   * @option option-name
   *   Description
   * @usage concurrent_queue_test-fillCharacterQueue 1
   *   Usage description
   *
   * @command concurrent_queue_test:fillCharacterQueue
   */
  public function fillCharacterQueue(int $filmId, $options = ['option-name' => 'default']) {
    $this->logger()->info(dt('Fetching info for film @filmId', ['@filmId' => $filmId]));
    $response =  $this->httpClient->request('GET', 'https://swapi.dev/api/films/' . $filmId);
    $body = $response->getBody()->getContents();
    $filmData = Json::decode($body);
    if (empty($filmData)) {
      $this->logger()->error(dt('Unexpected response for film response'));
      return;
    }
    $this->logger()->info(dt('Creating queue items for @count characters.', ['@count' => count($filmData['characters'])]));

    foreach ($filmData['characters'] as $url) {
      $this->charactersQueue->createItem([
        'url' => $url,
      ]);
    }
  }

  /**
   * An example of the table output format.
   *
   * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @field-labels
   *   group: Group
   *   token: Token
   *   name: Name
   * @default-fields group,token,name
   *
   * @command concurrent_queue_test:token
   * @aliases token
   *
   * @filter-default-field name
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function token($options = ['format' => 'table']) {
    $all = \Drupal::token()->getInfo();
    foreach ($all['tokens'] as $group => $tokens) {
      foreach ($tokens as $key => $token) {
        $rows[] = [
          'group' => $group,
          'token' => $key,
          'name' => $token['name'],
        ];
      }
    }
    return new RowsOfFields($rows);
  }
}
