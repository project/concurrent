<?php

namespace Drupal\concurrent_queue_test\Plugin\QueueWorker;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Defines 'concurrent_queue_test_swapi_characters' queue worker.
 *
 * @QueueWorker(
 *   id = "concurrent_queue_test_swapi_characters",
 *   title = @Translation("Swapi Characters"),
 *   cron = {"time" = 60}
 * )
 */
class SwapiCharactersWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface{

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \GuzzleHttp\ClientInterface $client
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $client, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    $id = $this->getIdFromUrl($item['url']);
    if ($id === NULL) {
      return;
    }
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $data = $this->fetch($item['url']);
    $results = $nodeStorage
      ->getQuery()
      ->condition('type', 'character')
      ->condition('field_swapi_id', $id)
      ->execute();
    if (!empty($results)) {
      $node = $nodeStorage->load(reset($results));
    }
    else {
      $node = $nodeStorage->create([
        'type' => 'character',
        'field_swapi_id' => $id,
      ]);
    }
    $node->get('title')->setValue($data['name']);
    $node->get('field_birth_year')->setValue($data['birth_year']);
    $homeworld = $this->fetch($data['homeworld']);
    if (!empty($homeworld)) {
      $node->get('field_homeworld')->setValue($homeworld['name']);
    }
    $node->save();
  }
  protected function fetch(string $url): ?array {
    $response = $this->client->request('GET', $url);
    $data = Json::decode($response->getBody()->getContents());
    if (empty($data)) {
      return NULL;
    }

    return $data;
  }
  protected function getIdFromUrl(string $url): ?int {
    $pieces = explode('/', trim($url, '/'));
    $id = end($pieces);
    if (!is_numeric($id)) {
      return NULL;
    }
    return (int) $id;
  }

}
